INTRODUCTION
------------

The Drupal HTML filter already have an option to add *rel="nofollow"*
attribute to outgoing links. However, if you use any other filter that
creates URL (e.g. a plain text input format with core URL filter),
then there's no way to add the *rel* tag to the these created links.

Also, the core filter is tied to a set of tags that should be not be
removed from the HTML. You *must* specify the tags, otherwise all tags
are removed.

This module fills this gap. It provides a basic input filter that will
add *rel="nofollow"* to outgoing links. So, for instance in the
example above, just add this filter after the core URL filter and
you're done.



REQUIREMENTS
------------

None



INSTALLATION
------------

* Install module as usual. See
  https://drupal.org/documentation/install/modules-themes/modules-7 for
  further information.

* Create/edit an input format on "Configuration >> Content authoring >> Text
  formats" (admin/config/content/formats)

* On the input format page, check the "Rearrange" tab and make sure
  that the **rel=nofollow Filter** comes after any other filters that
  generates HTML links.

* Make sure that the option _Add rel="nofollow" to all links_ is *not*
  enabled in the HTML filter (if you are using it).

* (Optional) Configure the filter to add URLs that should be ignored
  by the filter.



KNOWN BUGS
----------

This module has a name that is too funny. But it's just too late to
fix this. Case closed (won't fix).

For other bugs and issues, see the Issue Queue in Drupal.org.
